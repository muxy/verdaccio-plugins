FROM node:10.14.1-alpine

RUN apk --no-cache add curl make gcc g++ python

WORKDIR /plugins

RUN npm v verdaccio-bitbucket dist.tarball | xargs curl | tar -xz && \
	mv package verdaccio-bitbucket && \
	cd ./verdaccio-bitbucket && \
	npm install && \
	cd -

RUN npm v verdaccio-s3-storage dist.tarball | xargs curl | tar -xz && \
	mv package verdaccio-s3-storage && \
	cd ./verdaccio-s3-storage && \
	npm install && \
	cd -

RUN npm v verdaccio-github-oauth-ui dist.tarball | xargs curl | tar -xz && \
	mv package verdaccio-github-oauth-ui && \
	cd ./verdaccio-github-oauth-ui && \
	npm install && \
	cd -

FROM busybox:musl

COPY --from=0 /plugins /plugins